#!/bin/bash
PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"

args=("$@")
docker build -t generador-portadas-pdf . 
path_volume_input=$(readlink -m ${args[0]}) 
path_volume_output=$(readlink -m ${args[1]})
docker run --rm -t -i --volume ${path_volume_input}:/procesar --volume ${path_volume_output}:/output generador-portadas-pdf --reemplazar
