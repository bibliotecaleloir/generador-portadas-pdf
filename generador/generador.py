import os
import datetime
import xml.etree.ElementTree as ET
import argparse
import subprocess
import shutil
from glob import glob

from tqdm import tqdm, tnrange
import pdfkit
from PyPDF2 import PdfFileWriter, PdfFileReader, PdfFileMerger
from PyPDF2.utils import PdfReadError

import jinja2

'''
TENER EN CUENTA QUE PARA pdfkit se necesita wkhtmltopdf
https://pypi.python.org/pypi/pdfkit
'''

def get_metadata(carpeta):
    '''
    A partir de una carpeta (path) devuelve un diccionario con los metadatos del documento
    que lee del archivo "metadata.xml" se se encuentra dentro de esa carpeta.
    '''
    xml_file_path = os.path.join(carpeta, 'metadata.xml')
    tree = ET.parse(xml_file_path)
    root = tree.getroot()

    metadata = {}

    # https://docs.python.org/3/library/xml.etree.elementtree.html#xpath-support
    metadata['ArchiveName'] = root.find('.//FileSet/Description/Metadata[@name="dl.ArchiveName"]').text
    metadata['Creator'] = root.find('.//FileSet/Description/Metadata[@name="dl.Creator"]').text
    metadata['Title'] = root.find('.//FileSet/Description/Metadata[@name="dl.Title"]').text
    metadata['Date_Created'] = anio_defensa = root.find('.//FileSet/Description/Metadata[@name="dl.Date_Created"]').text
    metadata['Identifier_URL'] = root.find('.//FileSet/Description/Metadata[@name="dl.Identifier_URL"]').text
    metadata['Type'] = root.find('.//FileSet/Description/Metadata[@name="dl.Type"]').text
    
    keywords_xpath = root.findall('.//FileSet/Description/Metadata[@name="dl.Subject_pc"]')
    keywords = [k.text for k in keywords_xpath if k != None]
    #metadata['Keywords'] = ' '.join([k.text for k in keywords])
    metadata['Keywords'] = keywords
    return metadata


def gen_html_caratula(carpeta):
 
    templateLoader = jinja2.FileSystemLoader( searchpath="./templates")
    templateEnv = jinja2.Environment( loader=templateLoader )
    TEMPLATE_FILE = "portada.html"
    template = templateEnv.get_template( TEMPLATE_FILE )
    
  
    context = get_metadata(carpeta)
    base_path = '<base href="/usr/src/app/templates/">'
    html = template.render(metadata=context, base_path=base_path)
    return html

def agregar_metadata_a_pdf(carpeta, pdf_path):
    
    metadatos_xml = get_metadata(carpeta)
    metadatos_pdf = [
        '-XMP-dc:Creator="{}"'.format(metadatos_xml['Creator']),
        '-XMP-dc:Publisher="Facultad de Ciencias Exactas y Naturales. Universidad de Buenos Aires. http://digital.bl.fcen.uba.ar"',
        '-XMP-dc:Date="{}"'.format(metadatos_xml['Date_Created']),
        '-XMP-dc:Title="{}"'.format(metadatos_xml['Title']),
        '-XMP-pdf:Keywords="{}"'.format(metadatos_xml['Keywords']),
    ]
    exiftool_line = ["exiftool", "-quiet", "-overwrite_original"] + metadatos_pdf + [pdf_path]
    subprocess.run(exiftool_line, check=True)

def gen_caratula(carpeta, carpeta_output, ancho=210.0):
    '''
    Crea un archivo "caratula.pdf" en la carpeta (path) que se pasa como parámetro.
    En la carpeta se espera que haya un archivo llamado "metadata.xml", y
    un archivo llamado igual que la carpeta + ".pdf"
    El parámetro ancho es el ancho de la portada en mm. El alto se calcula
    automáticamente manteniendo la relación de aspecto de A4 (210x297)
    '''
    html = gen_html_caratula(carpeta)

    alto = (ancho*297.0)/210.0

    options = {
        'page-width': f'{ancho:.2f}mm',
        'page-height': f'{alto:.2f}mm',
        'margin-top': '0.25mm',
        'margin-right': '0.25mm',
        'margin-bottom': '0.25mm',
        'margin-left': '0.25mm',
        'encoding': "UTF-8",
        'no-outline': None,
        'quiet': '',
    }
    abspathCaratula = os.path.join(carpeta_output, 'caratula.pdf')

    pdfkit.from_string(html,abspathCaratula, options)

    caratula_reader = PdfFileReader(abspathCaratula)
    if caratula_reader.getNumPages() != 1 and ancho<210.0:
        ## la caratula no entró. reintentamos generarla en A4
        ancho = 210.0
        alto = 297.0
        options['page-width'] = f'{ancho:.2f}mm'
        options['page-height'] = f'{alto:.2f}mm'
        pdfkit.from_string(html,abspathCaratula, options)
        caratula_reader = PdfFileReader(abspathCaratula)
        if caratula_reader.getNumPages() != 1:
            tqdm.write(f'AVISO: la caratula de {carpeta} no entró en una sola página.')

    return abspathCaratula





def agregar_caratula_alt(carpeta_input, carpeta_output, reemplazar=False):

    pdf_original_filename = os.path.join(carpeta_input, os.path.basename(carpeta_input))+'.pdf'
    pdf_output_filename = os.path.join(carpeta_output, os.path.basename(carpeta_input))+'.pdf'

    os.makedirs(os.path.dirname(pdf_output_filename), exist_ok=True)
    pdf_desencriptado_filename = os.path.splitext(pdf_output_filename)[0] + '_desencriptado.pdf'
    subprocess.run(['qpdf', '--decrypt', pdf_original_filename, pdf_desencriptado_filename], check=True)
    
    with open(pdf_desencriptado_filename, 'rb') as pdf_desencriptado_f:
        pdf_desencriptado_reader = PdfFileReader(pdf_desencriptado_f)
        if pdf_desencriptado_reader.isEncrypted:
            try:
                pdf_desencriptado_reader.decrypt('')
            except NotImplementedError:
                # en esos casos generamos una caratula con tamaño A4
                caratula_path = gen_caratula(carpeta_input, carpeta_output)
        else:
            try:
                primera_pagina_original = pdf_desencriptado_reader.getPage(0)
                ancho_caratula = float(primera_pagina_original.trimBox.getWidth()) / 2.83   # PyPDF2 me da el valor en pts en vez de en mm
                caratula_path = gen_caratula(carpeta_input, carpeta_output, ancho_caratula)
            except PdfReadError:
                # en esos casos generamos una caratula con tamaño A4
                caratula_path = gen_caratula(carpeta_input, carpeta_output)
                         
    # mergear caratula y PDF
    if (reemplazar):
        subprocess.run(['qpdf', pdf_desencriptado_filename, "--pages", caratula_path, "1-1", pdf_desencriptado_filename, "2-z", "--", pdf_output_filename], check=True)
    else:
        subprocess.run(['qpdf', pdf_desencriptado_filename, "--pages", caratula_path, "1-1", pdf_desencriptado_filename, "--", pdf_output_filename], check=True)
    agregar_metadata_a_pdf(carpeta_input, pdf_output_filename)
    # tratar de ir a PDF/a
    shutil.move(pdf_output_filename, pdf_output_filename + '_tmp')
    #subprocess.run(['qpdf', pdf_output_filename + '_tmp', "--min-version=1.7", "--newline-before-endstream", pdf_output_filename], check=True)
    subprocess.run(['qpdf', pdf_output_filename + '_tmp', "--min-version=1.7", pdf_output_filename], check=True)
    # borrar caratula, desencriptado y tmp
    os.remove(caratula_path)
    os.remove(pdf_desencriptado_filename)
    os.remove(pdf_output_filename + '_tmp')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = 'Agregar las carátulas a los PDFs de un import de Greenstone.')
    parser.add_argument('directorio_input', help='Directorio del import. Se espera que dentro de ese directorio haya una carpeta por cada documento. Dentro de cada carpeta, debe haber un PDF y un metadata.xml. El PDF se debe llamar igual que la carpeta.')
    parser.add_argument('directorio_output', help='Nombre del directorio de salida, donde se guardarán los PDFs con la carátula.')
    parser.add_argument('--reemplazar', action='store_true', default=False)
    args = parser.parse_args()

    for carpeta in tqdm(next(os.walk(args.directorio_input))[1], desc="documentos"):
        #print("Agregando carátula a {}".format(carpeta))
        carpeta_input = os.path.join(args.directorio_input, carpeta)
        carpeta_output = os.path.join(args.directorio_output, carpeta)
        agregar_caratula_alt(carpeta_input, carpeta_output, args.reemplazar)
        
        metadataxml_path_original = os.path.join(carpeta_input, 'metadata.xml')
        metadataxml_path_output = os.path.join(carpeta_output, 'metadata.xml')
        shutil.copyfile(metadataxml_path_original, metadataxml_path_output)
