FROM python:3.6-slim-jessie
LABEL maintainer "Pablo Montepagano <pablo@montepagano.com.ar>"

ENV http_proxy http://proxy.fcen.uba.ar:8080

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
  libfontconfig1 \
  libxext6 \
  libxrender1 \
  libimage-exiftool-perl \
  pdftk \
  qpdf \
  wget \
  xz-utils

COPY build/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz /root/
WORKDIR /root
RUN tar xvJf wkhtmltox-0.12.4_linux-generic-amd64.tar.xz \
  && mv /root/wkhtmltox/lib/* /usr/lib/ \
  && mv /root/wkhtmltox/bin/* /usr/bin/ \
  && mv /root/wkhtmltox/include/* /usr/include/

COPY build/requirements.txt ./
RUN pip install -r requirements.txt

COPY generador /usr/src/app
WORKDIR /usr/src/app

ENTRYPOINT ["python", "/usr/src/app/generador.py", "/procesar", "/output"]
#CMD ["--help"]
